using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ConnectUNCWithCredentials
{
    public partial class TestConnect : Form
    {
        public TestConnect()
        {
            InitializeComponent();
            tbUNCPath.Text = @"\\DESKTOP-HU71HPC\Songs";
            tbUserName.Text = "username";
            tbDomain.Text = "DESKTOP-HU71HPC";
            tbPassword.Text = "password";
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();
            string[] dirs;
            using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
            {
                if (unc.NetUseWithCredentials(tbUNCPath.Text,
                                              tbUserName.Text,
                                              tbDomain.Text,
                                              tbPassword.Text))
                {
                    //int fileCount = 0;
                    dirs = Directory.GetDirectories(tbUNCPath.Text);
                    string[] files = Directory.GetFiles(tbUNCPath.Text, "*", SearchOption.AllDirectories);
                    tbDirList.Text += "File Count: " + files.Length.ToString();
                    foreach (string s in files)
                        tbDirList.Text += "\n File Path: " + s;

                    //foreach (string d in dirs)
                    //{
                    //    Directory.GetFiles(d);
                    //    tbDirList.Text += d + "\r\n";
                    //}
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Failed to connect to " + tbUNCPath.Text + "\r\nLastError = " + unc.LastError.ToString(),
                                    "Failed to connect",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }

            }
            this.Cursor = Cursors.Default;
        }
    }
}